<?php

// Add Tools page
add_action( 'admin_menu', function() {
    global $blog_id;

    // Only add on main site
    if ( ! $blog_id || ! is_main_site( $blog_id ) ) {
        return false;
    }

    // Add import management page
    add_management_page( 'Import People', 'Import People', 'import_ua_sa_people', 'import-ua-sa-people', 'import_ua_sa_people_page' );

}, 100 );

// Print Tools page
function import_ua_sa_people_page() {
    global $blog_id;

    // Only print on main site
    if ( ! $blog_id || ! is_main_site( $blog_id ) ) {
        return false;
    }

    ?><div class="wrap">
        <h2><?php echo esc_html( get_admin_page_title() ); ?></h2><?php

        // Will process import if needed, otherwise show form
        if ( ! process_ua_sa_people_import() ) {

            ?><form enctype="multipart/form-data" method="post" action="<?php echo admin_url( 'tools.php?page=import-ua-sa-people' ); ?>">
                <?php wp_nonce_field( 'importing_ua_sa_people', 'import_ua_sa_people_nonce' ); ?>
                <p>Choose the file from your computer with the people import information.</p>
                <input type="file" name="ua_sa_people_import_file"/>

                <table class="form-table">
                    <tbody>
                        <tr>
                            <th scope="row"><label for="ua_sa_people_import_offset">People Offset</label></th>
                            <td><input name="ua_sa_people_import_offset" type="number" id="ua_sa_people_import_offset" value="" /></td>
                        </tr>
                        <tr>
                            <th scope="row"><label for="ua_sa_people_import_limit">People Limit</label></th>
                            <td><input name="ua_sa_people_import_limit" type="number" id="ua_sa_people_import_limit" value="" /></td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <input type="submit" class="button primary" value="Import"/>
            </form><?php

        }

    ?></div><?php

}

// Manage import
function process_ua_sa_people_import() {
    global $wpdb, $blog_id, $cpt_onomy;

    // Only process on main site
    if ( ! $blog_id || ! is_main_site( $blog_id ) ) {
        return false;
    }

    // Check the nonce
    if ( ! ( isset( $_POST[ 'import_ua_sa_people_nonce' ] ) && wp_verify_nonce( $_POST[ 'import_ua_sa_people_nonce' ], 'importing_ua_sa_people' ) ) ) {
        return false;
    }

    // Make sure we have a file
    if ( ! ( $import_file = ! empty( $_FILES ) && isset( $_FILES[ 'ua_sa_people_import_file' ] ) ? $_FILES[ 'ua_sa_people_import_file' ] : false ) ) {
        return false;
    }

    // Make sure it's a legit file
    if ( ! is_uploaded_file( $import_file[ 'tmp_name' ] ) ) {
        return false;
    }

    // Make sure we have the right type
    if ( strcasecmp( 'text/csv', $import_file[ 'type' ] ) != 0 ) {
        return false;
    }

    // Get the CSV file
    $csv_file = $import_file[ 'tmp_name' ];

    // Open the CSV file
    if ( ( $csv_file_handle = fopen( $csv_file, 'r' ) ) !== false ) {

        // Keep track of the columns and people data
        $people_columns = array();
        $people_data = array();

        // Remember which column holds certain values
        $prefix_col_index = false;
        $suffix_col_index = false;
        $mybama_col_index = false;
        $cwid_col_index = false;

        // Will hold data
        $prefixes = array();
        $suffixes = array();
        $mybama_ids = array();

        // Will hold errors if they exist
        $people_data_errors = array();

        // Keep track of the row
        $row = 0;

        // Loop through the data
        while ( ( $csv_file_data = fgetcsv( $csv_file_handle ) ) !== false ) {

            // How many columns in this row?
            $num_of_columns = count( $csv_file_data );

            // Make sure there are 13 columns
            /*if ( $num_of_columns != 13 ) {
                $people_data_errors[] = "ERROR: There were {$num_of_columns} fields in line {$row}:";
            }*/

            // Create array for each person
            $this_person = array();

            //echo "\n\n<Br /><br />{$num_of_columns} fields in line {$row}:";
            for ( $c = 0; $c < $num_of_columns; $c++ ) {

                // Get cell value
                $cell = $csv_file_data[$c];

                // If first row, get the columns
                if ( 0 == $row ) {

                    // Process myBama and move to front
                    if ( strcasecmp( 'MYBAMA', $cell ) == 0
                        || strcasecmp( 'MYBAMA ID', $cell ) == 0 ) {

                        // Set the index
                        $mybama_col_index = $c;

                        // Add to the front of the pack
                        //array_unshift( $people_columns, $cell );

                    }

                    // Take care of everything else
                    else {

                        // Remember which column holds the prefix
                        if (strcasecmp('PREFIX', $cell) == 0) {
                            $prefix_col_index = $c;
                        }

                        // Remember which column holds the suffix
                        else if (strcasecmp('SUFFIX', $cell) == 0) {
                            $suffix_col_index = $c;
                        }

                        // Don't process the CWID
                        else if (strcasecmp('CWID', $cell) == 0) {
                            $cwid_col_index = $c;
                            continue;
                        }

                    }

                    // Add the column
                    $people_columns[] = $cell;

                } else {

                    if ( ! isset( $mybama_col_index ) ) {
                        $people_data_errors[] = "ERROR: There is no myBama index.";
                    }

                    // Process myBama and move to front
                    if ( isset( $mybama_col_index ) && $c == $mybama_col_index ) {

                        // Make sure they have a myBama
                        if ( empty( $cell ) ) {
                            $people_data_errors[] = "ERROR: Line {$row} does not have a myBama ID.";
                        } else {

                            // Make sure they don't have a duplicate myBama
                            if ( in_array( $cell, $mybama_ids ) ) {
                                $people_data_errors[] = "ERROR: Line {$row} has a duplicate myBama ID.";
                            } else {

                                // Store the ID
                                $mybama_ids[] = $cell;

                                // Add to the front of the pack
                                //array_unshift( $this_person, $cell );

                                // Add the cell value
                                $this_person[] = $cell;

                                //continue;

                            }

                        }

                    }

                    // Take care of everything else
                    else {

                        // Don't process the CWID
                        if (isset($cwid_col_index) && $c == $cwid_col_index) {
                            continue;
                        }

                        // Deal with the prefix
                        else if (isset($prefix_col_index) && $c == $prefix_col_index) {

                            // Store the prefixes
                            if ( ! empty($cell)) {

                                // Clean up the prefix
                                if (preg_match('/^(Dr|Mr|Mrs|Ms)$/i', $cell)) {
                                    $cell .= '.';
                                } else if (strcasecmp('Miss.', $cell) == 0) {
                                    $cell = 'Miss';
                                }

                                // Add the prefix
                                $prefixes[] = $cell;

                            }

                        }

                        // Deal with the suffix
                        else if (isset($suffix_col_index) && $c == $suffix_col_index) {

                            // Store the suffixes
                            if ( ! empty($cell)) {
                                $suffixes[] = $cell;
                            }

                        }

                        // Add the cell value
                        $this_person[] = $cell;

                    }

                }

            }

            // Add person to people
            if ( $row > 0 ) {
                $people_data[] = $this_person;
            }

            // Keep track of each row
            $row++;

        }

        // Close the file
        fclose( $csv_file_handle );

        if ( ! empty( $people_data_errors ) ) {

            echo "\n\n<br /><Br />ERRORS:<pre>";
            print_r( $people_data_errors );
            echo "</pre>";
            exit;

        } else {

            echo "\n<br /><strong>NO ERRORS! YAY!</strong>";

            // Filter/clean up prefixes
            /*$prefixes = array_values( array_unique( $prefixes ) );
            echo "\n\n<br /><br />prefixes:<pre>";
            print_r( $prefixes );
            echo "</pre>";*/

            // Filter/clean up suffixes
            /*$suffixes = array_values( array_unique( $suffixes ) );
            echo "<pre>";
            print_r( $suffixes );
            echo "</pre>";*/

            /*echo "\n\n<br /><br />myBama:<pre>";
            print_r( $mybama_ids );
            echo "</pre>";*/

            // Keep track of who already exists
            $person_already_exists = 0;

            // Keep track of processed people
            $people_index = -1;
            $processed_people = 0;

            // Set some parameters
            $processed_people_offset = isset( $_POST[ 'ua_sa_people_import_offset' ] ) && $_POST[ 'ua_sa_people_import_offset' ] > 0 ? $_POST[ 'ua_sa_people_import_offset' ] : 0;
            $processed_people_limit = isset( $_POST[ 'ua_sa_people_import_limit' ] ) && $_POST[ 'ua_sa_people_import_limit' ] > 0 ? $_POST[ 'ua_sa_people_import_limit' ] : 0;

            // Go through each person and create a post
            foreach( $people_data as $person ) {

                // Keep track of people whether the're processed or not
                $people_index++;

                // Stop processing at the limit
                if ( $processed_people_limit > 0 && $processed_people >= $processed_people_limit ) {
                    break;
                }

                // Start processing with the offset
                if ( $processed_people_offset > 0 && $people_index < $processed_people_offset ) {
                    continue;
                }

                // Reset the person ID
                $person_id = false;

                // We need their first and last name for the post title
                $fname = $lname = false;

                // We need their myBama ID
                $mybama_id = false;

                // Make sure the person keys is reset
                $person = array_values( $person );

                foreach( $person as $person_col => $person_val ) {

                    // Make sure they have a myBama ID
                    if ( 0 == $person_col ) {

                        if ( empty( $person_val ) ) {
                            echo "<br /><br />\n\nERROR: THIS PERSON HAS NO MYBAMA ID<br /><br />\n\n";
                            continue 2;
                        } else {

                            // Store the myBama ID
                            $mybama_id = $person_val;

                            echo "<br /><br /><hr />\n\nMYBAMA ID: {$mybama_id}";

                            // Check to see if this person already exists
                            $person_id = $wpdb->get_var( "SELECT meta.post_id FROM {$wpdb->postmeta} meta INNER JOIN {$wpdb->posts} posts ON posts.ID = meta.post_id AND posts.post_type = 'people' WHERE meta.meta_key = 'mybama' AND meta.meta_value LIKE '$mybama_id'" );

                            // Means this person already exists
                            if ( $person_id > 0 ) {
                                $person_already_exists++;
                            }

                            // If no matching ID, then make a new post
                            else {

                                echo "<br /><br />\n\nCREATE PERSON";

                                // Create person
                                $person_id = wp_insert_post( array(
                                    'post_status'   => 'publish',
                                    'post_type'     => 'people',
                                    'post_title'    => 'Person',
                                    'post_content'  => '',
                                    'post_name'     => $mybama_id,
                                ) );

                                // Store myBama ID
                                if ( $person_id > 0 ) {
                                    echo "<br />\nCREATE PERSON";
                                    echo "<br />\nadd mybama post meta!";
                                    add_post_meta( $person_id, 'mybama', $mybama_id, true );
                                }

                            }

                        }

                    }

                    else {

                        // Must have a person ID before continuing
                        if ( ! ($person_id > 0)) {
                            continue 2;
                        }

                        echo "\n<br />[" . $people_columns[ $person_col ] . "]: {$person_val}";

                        switch (strtolower(preg_replace('/[^a-z0-9]/i', '', $people_columns[ $person_col ]))) {

                            case 'prefix':
                                // Add/update the 'prefix' post meta
                                ua_sa_people_import_post_meta( $person_id, 'prefix', $person_val );
                                break;

                            case 'fname':

                                // Store for the post title
                                $fname = $person_val;

                                // Add/update the 'fname' post meta
                                ua_sa_people_import_post_meta( $person_id, 'fname', $person_val );

                                break;

                            case 'nickname':
                                // Add/update the 'pname' post meta
                                ua_sa_people_import_post_meta( $person_id, 'pname', $person_val );
                                break;

                            case 'middleinit':
                            case 'middleinitial':
                                // Add/update the 'mname' post meta
                                ua_sa_people_import_post_meta( $person_id, 'mname', $person_val );
                                break;

                            case 'lname':

                                // Store for the post title
                                $lname = $person_val;

                                // Add/update the 'lname' post meta
                                ua_sa_people_import_post_meta( $person_id, 'lname', $person_val );

                                break;

                            case 'suffix':
                                // Add/update the 'suffix' post meta
                                ua_sa_people_import_post_meta( $person_id, 'suffix', $person_val );
                                break;

                            case 'officephone':
                            case 'officetelephone':
                                // Add/update the 'phone' post meta
                                ua_sa_people_import_post_meta( $person_id, 'phone', preg_replace( '/[^0-9]/i', '', $person_val ) );
                                break;

                            case 'jobtitle':
                            case 'currtitle':
                                // Add/update the 'position' post meta
                                ua_sa_people_import_post_meta( $person_id, 'position', $person_val );
                                break;

                            case 'dept':
                            case 'deptname':

                                // Get the department name
                                $department = preg_replace( '/[\s]{2,}/i', ' ', $person_val );

                                // These names need to be changed
                                switch( $person_val ) {

                                    case 'Ferguson Center':
                                        $department = 'Ferguson Student Center';
                                        break;

                                    case 'HRC':
                                        $department = 'Housing and Residential Communities';
                                        break;

                                    case 'Student Life':
                                        $department = false;
                                        break;

                                    case "Women's Resource Center":
                                        $department = 'Women and Gender Resource Center';
                                        break;

                                }

                                // Set the department
                                if ( ! empty( $department ) ) {

                                    if ( $dept_term = $cpt_onomy->get_term_by( 'name', $department, 'departments' ) ) {

                                        echo "\n\n<br /><br />department: {$department}<pre>";
                                        print_r( $dept_term );
                                        echo "</pre>";

                                        // Set the relationship
                                        $cpt_onomy->wp_set_object_terms( $person_id, $dept_term->term_id, 'departments', true );

                                    } else {

                                        echo "<br /><Br />\n\nERROR: [[[[[NOT A VALID DEPARTMENT TERM: {$department} ]]]]]<br /><Br />\n\n";

                                    }

                                }

                                break;

                            case 'office':
                            case 'campaddr':
                                break;

                            case 'box':
                                // Add/update the 'box' post meta
                                ua_sa_people_import_post_meta( $person_id, 'box', preg_replace( '/[^0-9]/i', '', $person_val ) );
                                break;

                            case 'email':
                            case 'bamaemail':
                                // Add/update the 'email' post meta
                                ua_sa_people_import_post_meta( $person_id, 'email', $person_val );
                                break;

                            default:
                                echo "<br /><Br />\n\nERROR: [[[[[NOT VALID COLUMN]]]]]<br /><Br />\n\n";
                                break;

                        }

                    }

                }

                // Update the person with their name and myBama slug
                if ( $person_id > 0 ) {

                    // Build the name
                    $post_title_name = preg_replace( '/[\s]{2,}/i', ' ', "{$fname} {$lname}" );

                    // Setup the post args
                    $person_update_post_args = array(
                        'ID'            => $person_id,
                        'post_status'   => 'publish',
                        'post_type'     => 'people',
                        'post_content'  => '',
                    );

                    // Update the post title/name
                    If ( ! empty( $post_title_name ) ) {
                        $person_update_post_args[ 'post_title' ] = $post_title_name;
                    }

                    // Update the myBama ID
                    if ( ! empty( $mybama_id ) ) {
                        $person_update_post_args[ 'post_name' ] = $mybama_id;
                    }

                    // Update the post
                    wp_update_post( $person_update_post_args );

                }

                $processed_people++;

                echo "<br />\nprocessed count: {$processed_people}";

            }

        }

    }

    return true;

}

// Import specfic post meta
function ua_sa_people_import_post_meta( $person_id, $meta_key, $meta_value ) {
    global $wpdb, $blog_id;

    // Only process on main site
    if ( ! $blog_id || ! is_main_site( $blog_id ) ) {
        return false;
    }

    // See if the meta row already exists
    if ( $meta_exists = $wpdb->get_var( "SELECT EXISTS( SELECT 1 FROM {$wpdb->postmeta} WHERE post_id = {$person_id} AND meta_key = '{$meta_key}' )" ) ) {
        $did_update = update_post_meta( $person_id, $meta_key, $meta_value );
        //echo "<br />\nupdate [{$person_id}] [{$meta_key}] [{$meta_value}] - did it update? [{$did_update}]";
        return $did_update;
    } else {
        $did_add = add_post_meta( $person_id, $meta_key, $meta_value, true );
        //echo "<br />\nadd [{$person_id}] [{$meta_key}] [{$meta_value}] - did it add? [{$did_add}]";
        return $did_add;
    }

}