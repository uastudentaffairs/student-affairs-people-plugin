<?php

class Student_Affairs_Person {

	/**
	 * The person's post ID.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @param   int - $ID - holds the person's post ID
	 */
	public $ID;

	/**
	 * The person's post data.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @param   object - $post - holds the person's post data
	 */
	public $post;

	/**
	 * Warming up the Slack mobile.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @param   int - $post_id - the person's post ID
	 */
	public function __construct( $post_id ) {

		// No point if there's no post ID
		if ( ! ( $post_id > 0 ) ) {
			return false;
		}

		// Get the person info
		if ( ( $person_response = wp_remote_get( "https://sa.ua.edu/wp-json/wp/v2/people/{$post_id}" ) )
			&& ( $person = json_decode( wp_remote_retrieve_body( $person_response ) ) )
			&& isset( $person->ID ) && $person->ID == $post_id ) {

			// Store ID
			$this->ID = $post_id;

			// Store the post data
			$this->post = $person;

		} else {
			return false;
		}

	}

	/**
	 * Get the person's email address.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_email() {

		// Make sure we have the post ID
		if ( ! $this->ID ) {
			return false;
		}

		// See if the email is already set
		if ( isset( $this->email ) && ! empty( $this->email ) ) {
			return $this->email;
		}

		// See if in post
		if ( isset( $this->post->email ) && ! empty( $this->post->email ) ) {
			return $this->post->email;
		}

		// Get the email
		if ( $email = student_affairs_people()->get_person_email( $this->ID ) ) {

			// Store the email
			$this->email = $email;

			return $this->email;

		}

		return false;

	}

	/**
	 * Get a person's box number.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_box() {

		// Make sure we have the post ID
		if ( ! $this->ID ) {
			return false;
		}

		// See if the box is already set
		if ( isset( $this->box ) && ! empty( $this->box ) ) {
			return $this->box;
		}

		// See if in post
		if ( isset( $this->post->box ) && ! empty( $this->post->box ) ) {
			return $this->post->box;
		}

		// Get the box
		if ( $box = student_affairs_people()->get_person_box( $this->ID ) ) {

			// Store the box
			$this->box = $box;

			return $this->box;

		}

		return false;

	}

	/**
	 * Get a person's affiliation.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_affiliation() {

		// Make sure we have the post ID
		if ( ! $this->ID ) {
			return false;
		}

		// See if the affiliation is already set
		if ( isset( $this->affiliation ) && ! empty( $this->affiliation ) ) {
			return $this->affiliation;
		}

		// See if in post
		if ( isset( $this->post->affiliation ) && ! empty( $this->post->affiliation ) ) {
			return $this->post->affiliation;
		}

		// Get the affiliation
		if ( $affiliation = student_affairs_people()->get_person_affiliation( $this->ID ) ) {

			// Store the affiliation
			$this->affiliation = $affiliation;

			return $this->affiliation;

		}

		return false;

	}

	/**
	 * Get a person's position.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_position() {

		// Make sure we have the post ID
		if ( ! $this->ID ) {
			return false;
		}

		// See if the position is already set
		if ( isset( $this->position ) && ! empty( $this->position ) ) {
			return $this->position;
		}

		// See if in post
		if ( isset( $this->post->position ) && ! empty( $this->post->position ) ) {
			return $this->post->position;
		}

		// Get the position
		if ( $position = student_affairs_people()->get_person_position( $this->ID ) ) {

			// Store the position
			$this->position = $position;

			return $this->position;

		}

		return false;

	}

	/**
	 * Get the person's phone number.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_phone() {

		// Make sure we have the post ID
		if ( ! $this->ID ) {
			return false;
		}

		// See if the phone is already set
		if ( isset( $this->phone ) && ! empty( $this->phone ) ) {
			return $this->phone;
		}

		// See if in post
		if ( isset( $this->post->phone ) && ! empty( $this->post->phone ) ) {
			return $this->post->phone;
		}

		// Get the phone
		if ( $phone = student_affairs_people()->get_person_phone( $this->ID ) ) {

			// Store the phone
			$this->phone = $phone;

			return $this->phone;

		}

		return false;

	}

	/**
	 * Get a person's office.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_office() {

		// Make sure we have the post ID
		if ( ! $this->ID ) {
			return false;
		}

		// See if the office is already set
		if ( isset( $this->office ) && ! empty( $this->office ) ) {
			return $this->office;
		}

		// See if in post
		if ( isset( $this->post->office ) && ! empty( $this->post->office ) ) {
			return $this->post->office;
		}

		// Get the office
		if ( $office = student_affairs_people()->get_person_office( $this->ID ) ) {

			// Store the office
			$this->office = $office;

			return $this->office;

		}

		return false;
	}

	/**
	 * Get meta for the person.
	 *
	 * @access  public
	 * @since   1.0
	 */
	public function get_post_meta( $meta_key, $single = true ) {
		return get_post_meta( $this->ID, $meta_key, $single );
	}

}

/**
 * Returns an instance of our Student_Affairs_Person class.
 *
 * @since	1.0.0
 * @access	public
 * @return	Student_Affairs_Person
 * @param   int - $post_id - the person's post ID
 */
function get_student_affairs_person( $post_id ) {
	return new Student_Affairs_Person( $post_id );

	/*global $ua_eng_people;

	// See if this person has already been constructed
	if ( isset( $ua_eng_people ) && isset( $ua_eng_people[ $post_id ] )
		&& ( $person = $ua_eng_people[ $post_id ] )
		&& is_a( $person, 'UA_ENG_Person' )
		&& isset( $person->ID ) ) {

		return $person;

	// Construct, and store, the person
	} else if ( ( $person = new UA_ENG_Person( $post_id ) )
		&& isset( $person->ID ) ) {

		// Store the person
		$ua_eng_people[ $post_id ] = $person;

		return $person;

	}

	return false;*/

}