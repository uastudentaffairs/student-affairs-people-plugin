<?php

// Setup admin styles and scripts
add_action( 'admin_enqueue_scripts', function ( $hook_suffix ) {
	global $post_type;

	// Load depending on the page
	switch( $hook_suffix ) {

		case 'post.php':
		case 'post-new.php':

			// Only for pages
			if ( in_array( $post_type, array(  'page' ) ) ) {

				// Get the plugin directory
				$sa_people_dir = plugin_dir_url( dirname( __FILE__ ) );

				// Register our script
				wp_enqueue_script( 'sa-people-admin', $sa_people_dir . '/js/sa-people-admin.min.js', array( 'jquery', 'select2' ), false, true );

			}

			break;

	}

});

// Add meta boxes
add_action( 'add_meta_boxes', function ( $post_type, $post ) {

	// Does this page have the people listing template?
	if ( isset( $post->ID ) && 'template-people-listing.php' == get_post_meta( $post->ID, '_wp_page_template', true ) ) {

		// Add people listing meta box
		$hero_image_post_types = array( 'page' );
		if ( in_array( $post_type, $hero_image_post_types ) ) {
			add_meta_box( 'sa-people-listing', __( 'People Listing', 'uastudentaffairs' ), 'print_sa_people_meta_boxes', $post_type, 'normal', 'high' );
		}

	}

}, 1, 2 );

// Print meta boxes
function print_sa_people_meta_boxes( $post, $metabox ) {

	switch( $metabox[ 'id' ] ) {

		case 'sa-people-listing':

			// Get selected values
			$selected_departments = get_post_meta( $post->ID, '_sa_people_listing_department', true );
			$selected_students = get_post_meta( $post->ID, '_sa_people_listing_students', true );
			$selected_orderby = get_post_meta( $post->ID, '_sa_people_listing_orderby', true );

			?><p>This page will display a listing of Student Affairs people. Use the settings below to define which people you would like to display.</p>
			<table class="form-table">
				<tbody>
					<tr>
						<th scope="row"><label for="sa-people-listing-department">Department(s)</label></th>
						<td>
							<select id="sa-people-listing-department" name="sa_people_listing_department[]" style="width:100%;" data-selected="<?php echo is_array( $selected_departments ) ? implode( ',', $selected_departments ) : $selected_departments; ?>" multiple="multiple">
								<option value="">No specific department</option>
							</select>
						</td>
					</tr>
					<tr>
						<th scope="row">Students</th>
						<td>
							<fieldset>
								<legend class="screen-reader-text"><span>Students</span></legend>
								<label for="sa-people-listing-students"><input name="sa_people_listing_students" type="checkbox" id="sa-people-listing-students" value="1"<?php checked( $selected_students > 0 ); ?>> Would you like to display student workers?</label>
							</fieldset>
						</td>
					</tr>
					<tr>
						<th scope="row">Order by Affiliation</th>
						<td>
							<fieldset>
								<legend class="screen-reader-text"><span>Order by Affiliation</span></legend>
								<label for="sa-people-listing-orderby"><input name="sa_people_listing_orderby" type="checkbox" id="sa-people-listing-orderby" value="affiliation"<?php checked( 'affiliation' == $selected_orderby ); ?>> Would you like to order your people by affiliation and place the fac/staff at the top and students at the bottom?</label>
							</fieldset>
						</td>
					</tr>
				</tbody>
			</table><?php

			break;

	}

}

// Save posts
add_action( 'save_post', function( $post_id, $post, $update ) {

	// Pointless if $_POST is empty (this happens on bulk edit)
	if ( empty( $_POST ) ) {
		return;
	}

	// Disregard autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	switch( $post->post_type ) {

		case 'page':

			// Verify that we're updating a post
			if ( wp_verify_nonce( $_POST[ '_wpnonce' ], 'update-post_' . $_POST[ 'post_ID' ] ) ) {

				// Process each field
				foreach( array( 'sa_people_listing_department', 'sa_people_listing_students', 'sa_people_listing_orderby' ) as $field_key ) {

					// Make sure fields are set
					if ( isset( $_POST[ $field_key ] ) ) {

						// Sanitize user input
						$field_value = $_POST[ $field_key ];

						// Update/save value
						update_post_meta( $post_id, "_{$field_key}", $field_value );

					}

					// Otherwise, clear it out
					else {
						delete_post_meta( $post_id, "_{$field_key}" );
					}

				}

			}

			break;

	}

}, 10, 3 );