<?php

// Filter admin people depending on user role
// $sa_edit_person_check lets us ensure the person check only runs once
global $sa_edit_person_check;
$sa_edit_person_check = 0;
add_action( 'pre_get_posts', function( $query ) {
	global $wpdb, $pagenow, $typenow, $post_id, $sa_edit_person_check;

	// True if we're trying to add a person
	$adding_person = ( 'people' == $typenow  && 'post-new.php' == $pagenow );

	// If adding, make sure they can create
	if ( $adding_person && ! current_user_can( 'create_people' ) ) {
		wp_die( 'Due to the broad scope and complexities of the people directory, you do not have the capability to add people at this time. Please contact <a href="mailto:rmcarden@ua.edu">External Affairs</a> to add new people to the directory. Thank you.', 'Not capable of adding people', array( 'back_link' => true ) );
	}

	// True if we're editing a person
	$editing_person = ( 'people' == $typenow  && 'post.php' == $pagenow );

	// True if we're editing people
	$editing_people = ( 'people' == $query->get( 'post_type' ) && 'edit.php' == $pagenow );

	// Make sure we're adding or editing someone
	if ( ! ( $adding_person || $editing_person || $editing_people ) ) {
		return;
	}

	// Get user roles
	$current_user = wp_get_current_user();
	$roles = isset( $current_user->roles ) ? $current_user->roles : array();

	// Make sure they're a department administrator
	if ( ! in_array( 'department_administrator', $roles ) ) {
		return;
	}

	// If editing people, setup tax query for each department
	if ( $editing_people ) {

		// Get departments to check
		$departments = $wpdb->get_col( "SELECT post_name FROM {$wpdb->posts} WHERE post_type = 'departments' AND post_status = 'publish' ORDER BY post_name ASC" );

		// Build tax query
		$tax_query   = array();

		foreach ( $departments as $dept ) {

			// Assign the taxonomy
			if ( current_user_can( 'manage_' . str_replace( '-', '_', $dept ) ) ) {

				// Add to the query
				$tax_query[] = array(
					'taxonomy' => 'departments',
					'field'    => 'slug',
					'terms'    => $dept,
					'operator' => 'IN'
				);
			}

		}

		// Add to query
		if ( ! empty( $tax_query ) ) {

			// Set the query
			$query->set( 'tax_query', $tax_query );

			// Get out of here
			return;

		}

	}

	// If editing a person, make sure they have access
	else if ( $editing_person ) {

		// If no ID, get out of here
		// This is important because no ID when saving
		if ( ! $post_id ) {
			return;
		}

		// If we've already checked the person get out of here
		if ( $sa_edit_person_check ) {
			return;
		}

		// Will be true if user has cap
		$user_can_edit_person = false;

		// Need to figure out person's departments
		if ( $person_departments = wp_get_object_terms( $post_id, 'departments', array( 'fields' => 'slugs' ) ) ) {

			foreach( $person_departments as $dept ) {

				// Make sure user has role
				if ( current_user_can( 'manage_' . str_replace( '-', '_', $dept ) ) ) {
					$user_can_edit_person = true;
				}

			}

		}

		// We've checked the person
		$sa_edit_person_check++;

		// If user can edit, get out of here
		if ( $user_can_edit_person ) {
			return;
		}

	}

	// They don't have the capabilities
	wp_die( 'You do not have access to edit this person.', 'Not allowed', array( 'back_link' => true ) );

}, 100 );

// Restrict who can edit the featured image
add_filter( 'admin_post_thumbnail_html', function( $content, $post_id ) {

	// Get post type
	$post_type = get_post_type( $post_id );

	// Only for people
	if ( 'people' != $post_type ) {
		return $content;
	}

	// If the user is allowed to edit photos...
	if ( current_user_can( 'manage_people_image' ) ) {

		// Add instructions
		$content .= '<div style="background:#efefef;padding:15px;margin:18px 0 0 0;">
			<p style="margin:0 0 10px 0;font-size:14px;"><strong>Please ensure the featured image meets the following requirements:</strong></p>
			<ul style="margin-top:0;">
				<li class="wp-ui-highlight" style="padding:12px 15px;">Photo must be taken by UA Photography</li>
				<li class="wp-ui-highlight" style="padding:12px 15px;">Resolution set at 72 dpi</li>
				<li class="wp-ui-highlight" style="padding:12px 15px;">Larger than 500x500 pixels</li>
				<li class="wp-ui-highlight" style="padding:12px 15px;">No larger than 2000 pixels wide.</li>
			</ul>
			<p class="description">Once selected, click "Crop Featured Image", make sure you\'re on the "Thumbnail" tab and crop the photo for the directory.</p>
		</div>';

	}

	// If the user is not allowed to edit photos...
	else {

		// Show the thumbnail
		$content = wp_get_attachment_image( get_post_thumbnail_id( $post_id ), 'thumbnail' );

		// Show the message
		$content .= '<p class="description">Please contact <a href="mailto:rmcarden@ua.edu">External Affairs</a> to add or change a person\'s directory photo.</p>';

	}

	return $content;
}, 100, 2 );

// Setup admin columns
add_filter( 'manage_people_posts_columns', function( $posts_columns ) {

	// Set the columns we want to add
	$add_columns_after_title = array(
		'position'  => 'Position',
		'email'     => 'Email',
		'office'    => 'Office',
	);

	// If there's no title column, add our columns to the end
	if ( ! array_key_exists( 'title', $posts_columns ) ) {
		return array_merge( $posts_columns, $add_columns_after_title );
	}

	// Create a new set of columns
	$new_columns = array();

	foreach ( $posts_columns as $key => $label ) {

		// Add the other columns
		$new_columns[ $key ] = $label;

		// Add our columns after the title
		if ( 'title' == $key ) {
			$new_columns = array_merge( $new_columns, $add_columns_after_title );
		}

	}

	return $new_columns;

}, 100 );

// Populate the columns
add_action( 'manage_people_posts_custom_column', function( $column_name, $post_id ) {

	switch( $column_name ) {

		case 'email':
			if ( $email = get_post_meta( $post_id, 'email', true ) ) {
				?><a href="mailto:<?php echo antispambot( $email ); ?>"><?php echo antispambot( $email ); ?></a><?php
			}
			break;

		case 'office':

			// Get building and room
			$office_room = trim( get_post_meta( $post_id, 'office_room', true ) );
			$office_building = trim( get_post_meta( $post_id, 'office_building', true ) ) ;

			if ( ! empty( $office_building ) ) {
				echo ! empty( $office_room ) ? "{$office_room} {$office_building}" : $office_building;
			}

			break;

		default:
			if ( $value = get_post_meta( $post_id, $column_name, true ) ) {
				echo $value;
			}
			break;

	}

}, 100, 2 );

// Add People Reports page
add_action('admin_menu', function() {
	add_submenu_page( 'edit.php?post_type=people', 'Reports', 'Reports', 'view_sa_people_reports', 'sa-people-reports', 'print_sa_people_admin_reports' );
});

// Print People Reports page
function print_sa_people_admin_reports() {
	global $wpdb;

	?><div class="wrap">
		<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

		<h3>People With No Photo</h3><?php

		$people_with_no_photo = $wpdb->get_results( "SELECT posts.*
			FROM {$wpdb->posts} posts
			WHERE post_type = 'people' AND post_status = 'publish'
			AND NOT EXISTS( SELECT meta_value FROM {$wpdb->postmeta} meta WHERE meta.post_id = posts.ID AND meta.meta_key = '_thumbnail_id' AND meta.meta_value != '' )
			ORDER BY posts.post_title ASC" );

		if ( ! $people_with_no_photo ) {
			?><p><em>Everyone has a photo.</em> Wow.</p><?php
		}

		else {
			?><p><em>There are <?php echo count( $people_with_no_photo ); ?> without a photo.</em></p>
			<ol style="margin-left: 30px;"><?php
				foreach( $people_with_no_photo as $person ) {
					?><li><a href="<?php echo get_edit_post_link( $person->ID ); ?>"><?php echo $person->post_title; ?></a></li><?php
				}
			?></ol><?php
		}

	?></div><?php

}