<?php

// Filter people's titles
/*add_filter( 'the_title', function( $person_title, $person_id ) {
	global $post;

	// Not in the admin
	if ( is_admin() ) {
		return $person_title;
	}

	return $person_title;
}, 100, 2 );*/

// Add some query vars
add_filter( 'query_vars', function( $query_vars ) {

	// This lets us know whether or not we should include students in the people queries
	// We do not get students by default
	$query_vars[] = 'students';

	return $query_vars;

});

// Tweak the people queries to "join" name information
add_filter( 'posts_clauses', 'ua_sa_people_query_join_info', 10, 2 );
function ua_sa_people_query_join_info( $pieces, $query ) {
	global $wpdb;

	// Only for people query
	$post_type = $query->get( 'post_type' );
	if ( 'people' == $post_type
	     || ( is_array( $post_type ) && in_array( 'people', $post_type ) && count( $post_type ) == 1 ) ) {

		// Only need to run if no set order or ordering by title
		/*$orderby = $query->get( 'orderby' );
		if ( ! empty( $orderby ) && strcasecmp( 'title', $orderby ) != 0 ) {
			return $pieces;
		}*/

		// Join to get name info
		foreach( array( 'prefix', 'fname', 'pname', 'mname', 'lname', 'suffix', 'affiliation' ) as $name_part ) {

			// Might as well store the join info as fields
			$pieces[ 'fields' ] .= ", {$name_part}.meta_value AS {$name_part}";

			// "Join" to get the info
			$pieces[ 'join' ] .= " LEFT JOIN {$wpdb->postmeta} {$name_part} ON {$name_part}.post_id = {$wpdb->posts}.ID AND {$name_part}.meta_key = '{$name_part}'";

		}

	}

	return $pieces;
}

// Tweak the people queries to order by name
add_filter( 'posts_clauses', 'ua_sa_people_query_orderby_name', 100, 2 );
function ua_sa_people_query_orderby_name( $pieces, $query ) {

	// Only for people query
	$post_type = $query->get( 'post_type' );
	if ( 'people' == $post_type
	     || ( is_array( $post_type ) && in_array( 'people', $post_type ) && count( $post_type ) == 1 ) ) {

		// Make sure the info has been joined
		if ( ! has_filter( 'posts_clauses', 'ua_sa_people_query_join_info' ) ) {
			$pieces = ua_sa_people_query_join_info( $pieces, $query );
		}

		// Get the set orderby
		$orderby = $query->get( 'orderby' );

		// Figure out the order
		$order = 'ASC'; //( $query_order = strtolower( $query->get( 'order' ) ) ) && in_array( $query_order, array( 'asc', 'desc' ) ) ? strtoupper( $query_order ) : 'ASC';

		// Setup the orderby
		$pieces[ 'orderby' ] = '';

		// Do we want to order by affiliation?
		if ( 'affiliation' == $orderby ) {
			$pieces[ 'orderby' ] .= " IF ( affiliation.meta_value IS NULL OR affiliation.meta_value NOT LIKE 'student', 1, 0 ) DESC,";
		}

		// We'll always order by name
		$pieces[ 'orderby' ] .= " lname.meta_value {$order}, fname.meta_key {$order}, mname.meta_value {$order}";

	}

	return $pieces;
}

// Tweak the people queries to not get students unless specified with 'students' query var
add_filter( 'posts_clauses', 'ua_sa_people_do_not_get_students', 100, 2 );
function ua_sa_people_do_not_get_students( $pieces, $query ) {

	// Not in the admin
	if ( is_admin() ) {
		return $pieces;
	}

	// Only for people query
	$post_type = $query->get( 'post_type' );
	if ( 'people' == $post_type
	    || ( is_array( $post_type ) && in_array( 'people', $post_type ) && count( $post_type ) == 1 ) ) {

		// Make sure the info has been joined
		if ( ! has_filter( 'posts_clauses', 'ua_sa_people_query_join_info' ) ) {
			$pieces = ua_sa_people_query_join_info( $pieces, $query );
		}

		// Unless specifically set, don't get students
		$get_students = $query->get( 'students' );
		if ( ! ( $get_students > 0 ) ) {
			$pieces[ 'where' ] .= " AND ( affiliation.meta_value IS NULL OR affiliation.meta_value NOT LIKE 'student')";
		}

	}

	return $pieces;
}