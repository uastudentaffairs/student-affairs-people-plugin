<?php

class Student_Affairs_People_API {

	/**
	 * Warming things up.
	 *
	 * @access  public
	 * @since   1.0.0
	 */
	public function __construct() {

		// Register any REST fields
		add_action( 'rest_api_init', array( $this, 'register_rest_fields' ) );

	}

	/**
	 * Register additional fields for the REST API.
	 *
	 * @access  public
	 * @since   1.0.0
	 */
	public function register_rest_fields() {

		// Set default args
		$field_args = array(
			'get_callback'    => array( $this, 'get_field_value' ),
			'update_callback' => null,
			'schema'          => null,
		);

		// Register fields
		$fields = array( 'thumbnail','departments','affiliation','affiliation_label','position','email','phone','box','office' );
		foreach( $fields as $field ) {
			register_rest_field( 'people', $field, $field_args );
		}

	}

	/**
	 * Get field values for the REST API.
	 *
	 * @param	array - $object - details of current post
	 * @param	string - $field_name - name of field
	 * @param	WP_REST_Request - $request - current request
	 * @return	mixed
	 */
	public function get_field_value( $object, $field_name, $request ) {

		switch( $field_name ) {

			case 'thumbnail':
				if ( $grid_photo_src = wp_get_attachment_image_src( get_post_thumbnail_id( $object['id'] ), 'thumbnail' ) ) {
					return $grid_photo_src[0];
				}
				break;

			case 'departments':
				if ( $departments = get_the_term_list( $object['id'], 'departments', '', ', ' ) ) {
					return $departments;
				}
				break;

			case 'affiliation':
			case 'affiliation_label':

				// Get affiliation
				$affiliation = student_affairs_people()->get_person_affiliation( $object['id'] );

				// Return affiliation or label
				if ( 'affiliation' == $field_name ) {
					return ( '' != $affiliation ) ? $affiliation : 'fac_staff';
				} else {
					return ( 'student' == $affiliation ) ? 'Student' : 'Faculty/Staff';
				}
				break;

			case 'position':
				if ( $position = student_affairs_people()->get_person_position( $object['id'] ) ) {
					return $position;
				}
				break;

			case 'email':
				if ( $email = student_affairs_people()->get_person_email( $object['id'] ) ) {
					return $email;
				}
				break;

			case 'phone':
				if ( $phone = student_affairs_people()->get_person_phone( $object['id'] ) ) {
					return $phone;
				}
				break;

			case 'box':
				if ( $box = student_affairs_people()->get_person_box( $object['id'] ) ) {
					return $box;
				}
				break;

			case 'office':
				if ( $office = student_affairs_people()->get_person_office( $object['id'] ) ) {
					return $office;
				}
				break;

		}

		return '';
	}

}

// Let's get this show on the road
new Student_Affairs_People_API;