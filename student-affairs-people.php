<?php

/**
 * Plugin Name:       UA Student Affairs - People
 * Plugin URI:        https://sa.ua.edu/
 * Description:       @TODO This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0
 * Author:            Rachel Carden, UA Student Affairs
 * Author URI:        https://sa.ua.edu/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ua-sa-people
 * Domain Path:       /languages
 */

// If this file is called directly, abort
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Load the files
require_once plugin_dir_path( __FILE__ ) . 'includes/student-affairs-person-class.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/admin.php';

// Only need on main site
if ( $blog_id && is_main_site( $blog_id ) ) {
	require_once plugin_dir_path( __FILE__ ) . 'includes/filters.php';
	require_once plugin_dir_path( __FILE__ ) . 'includes/admin-main-site.php';
	require_once plugin_dir_path( __FILE__ ) . 'includes/import.php';
	require_once plugin_dir_path( __FILE__ ) . 'includes/api.php';
}

class Student_Affairs_People {

	/**
	 * Holds the class instance.
	 *
	 * @since	1.0.0
	 * @access	private
	 * @var		Student_Affairs_People
	 */
	private static $instance;

	/**
	 * Returns the instance of this class.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @return	Student_Affairs_People
	 */
	public static function instance() {
		if ( ! isset( static::$instance ) ) {
			$className = __CLASS__;
			static::$instance = new $className;
		}
		return static::$instance;
	}

	/**
	 * Warming up the engine.
	 *
	 * @access  public
	 * @since   1.0.0
	 */
	protected function __construct() {

		// Load our textdomain
		add_action( 'init', array( $this, 'textdomain' ) );

		// Runs on install
		register_activation_hook( __FILE__, array( $this, 'install' ) );

		// Runs when the plugin is upgraded
		add_action( 'upgrader_process_complete', array( $this, 'upgrader_process_complete' ), 1, 2 );

		// Register custom post types
		add_action( 'init', array( $this, 'register_custom_post_types' ), 1 );

	}

	/**
	 * Method to keep our instance from being cloned.
	 *
	 * @since	1.0.0
	 * @access	private
	 * @return	void
	 */
	private function __clone() {}

	/**
	 * Method to keep our instance from being unserialized.
	 *
	 * @since	1.0.0
	 * @access	private
	 * @return	void
	 */
	private function __wakeup() {}

	/**
	 * Runs when the plugin is installed.
	 *
	 * @access  public
	 * @since   1.0.0
	 */
	public function install() {

		// Register the custom post type
		$this->register_custom_post_types();

		// Flush the rewrite rules to start fresh
		flush_rewrite_rules();

	}

	/**
	 * Runs when the plugin is upgraded.
	 *
	 * @access  public
	 * @since   1.0.0
	 */
	public function upgrader_process_complete() {

		// Flush the rewrite rules to start fresh
		flush_rewrite_rules();

	}

	/**
	 * Internationalization FTW.
	 * Load our textdomain.
	 *
	 * @TODO Add language files
	 *
	 * @access  public
	 * @since   1.0.0
	 */
	public function textdomain() {
		load_plugin_textdomain( 'uastudentaffairs', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}

	/**
	 * Get a person's display name
	 *
	 * @access  public
	 * @since   1.0.0
	 * @param   int - $post_id - the person's post ID
	 * @param   array - $name_parts - if we pass any name parts
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_person_display_name( $post_id, $name_parts = array() ) {

		// Make sure its an array
		if ( is_object( $name_parts ) ) {
			$name_parts = (array) $name_parts;
		}

		// Get starting name parts
		$first_name = isset( $name_parts[ 'fname' ] ) ? $name_parts[ 'fname' ] : get_post_meta( $post_id, 'fname', true );
		$preferred_name = isset( $name_parts[ 'pname' ] ) ? $name_parts[ 'pname' ] : get_post_meta( $post_id, 'pname', true );

		// Build name - start with first name or preferred name
		$name = ! empty( $preferred_name ) ? $preferred_name : $first_name;

		// Add middle name
		$name .= ' ' . ( isset( $name_parts[ 'mname' ] ) ? $name_parts[ 'mname' ] : get_post_meta( $post_id, 'mname', true ) );

		// Add last name
		$name .= ' ' . ( isset( $name_parts[ 'lname' ] ) ? $name_parts[ 'lname' ] : get_post_meta( $post_id, 'lname', true ) );

		// Add prefix
		if ( $prefix = isset( $name_parts[ 'prefix' ] ) ? $name_parts[ 'prefix' ] : get_post_meta( $post_id, 'prefix', true ) ) {
			$name = "{$prefix} {$name}";
		}

		// Add suffix
		if ( $suffix = isset( $name_parts[ 'suffix' ] ) ? $name_parts[ 'suffix' ] : get_post_meta( $post_id, 'suffix', true ) ) {
			$name .= " {$suffix}";
		}

		// Clean up and return the name
		return preg_replace( '/\s{2,}/i', ' ', $name );
	}

	/**
	 * Get a person's affiliation.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @param   int - $post_id - the person's post ID
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_person_affiliation( $person_id ) {
		return get_post_meta( $person_id, 'affiliation', true );
	}

	/**
	 * Get a person's position.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @param   int - $post_id - the person's post ID
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_person_position( $person_id ) {
		return get_post_meta( $person_id, 'position', true );
	}

	/**
	 * Get a person's office.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @param   int - $post_id - the person's post ID
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_person_office( $person_id ) {

		// Build office
		$office = null;

		// Get building
		if ( $building = get_post_meta( $person_id, 'office_building', true ) ) {

			$office = $building;

			// Get/add room
			if ( $room = get_post_meta( $person_id, 'office_room', true ) ) {
				$office = "{$room} {$office}";
			}

		}

		return preg_replace( '/\s{2,}/i', ' ', $office );
	}

	/**
	 * Get a person's email address.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @param   int - $post_id - the person's post ID
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_person_email( $post_id ) {
		return get_post_meta( $post_id, 'email', true );
	}

	/**
	 * Get a person's phone number.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @param   int - $post_id - the person's post ID
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_person_phone( $post_id ) {
		if ( $phone = preg_replace( '/[^0-9]/i', '', get_post_meta( $post_id, 'phone', true ) ) ) {
			return preg_replace( '/([0-9]{3})([0-9]{3})([0-9]{4})/','(\1) \2-\3', preg_replace( '/[^0-9]/i', '', $phone ) );
		}
		return false;
	}

	/**
	 * Get a person's box number.
	 *
	 * @access  public
	 * @since   1.0.0
	 * @param   int - $post_id - the person's post ID
	 * @return  string|false - string if value, false otherwise
	 */
	public function get_person_box( $person_id ) {
		return get_post_meta( $person_id, 'box', true );
	}

	/**
	 * Register our custom post types.
	 *
	 * @access  public
	 * @since   1.0.0
	 */
	public function register_custom_post_types() {
		global $blog_id;

		// Only register on main site
		if ( ! $blog_id || ! is_main_site( $blog_id ) ) {
			return false;
		}

		// Register people CPT
		register_post_type( 'people', array(
			'labels' => array(
				'name'               => _x( 'People', 'post type general name', 'ua-sa-people' ),
				'singular_name'      => _x( 'Person', 'post type singular name', 'ua-sa-people' ),
				'menu_name'          => _x( 'People', 'admin menu', 'ua-sa-people' ),
				'name_admin_bar'     => _x( 'People', 'add new on admin bar', 'ua-sa-people' ),
				'add_new'            => _x( 'Add New', 'people', 'ua-sa-people' ),
				'add_new_item'       => __( 'Add New Person', 'ua-sa-people' ),
				'new_item'           => __( 'New Person', 'ua-sa-people' ),
				'edit_item'          => __( 'Edit Person', 'ua-sa-people' ),
				'view_item'          => __( 'View Person', 'ua-sa-people' ),
				'all_items'          => __( 'All People', 'ua-sa-people' ),
				'search_items'       => __( 'Search People', 'ua-sa-people' ),
				'parent_item_colon'  => __( 'Parent Person:', 'ua-sa-people' ),
				'not_found'          => __( 'No people found.', 'ua-sa-people' ),
				'not_found_in_trash' => __( 'No people found in Trash.', 'ua-sa-people' )
			),
			'public'                => true,
			'publicly_queryable'    => true,
			'exclude_from_search'   => false,
			'show_in_nav_menus'     => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'show_in_admin_bar'     => true,
			'menu_icon'             => 'dashicons-groups',
			'capabilities'          => array(
				'edit_post'         => 'edit_person',
				'read_post'         => 'read_person',
				'delete_post'       => 'delete_person',
				'edit_posts'        => 'edit_people',
				'edit_others_posts' => 'edit_others_people',
				'publish_posts'     => 'publish_people',
				'read_private_posts'=> 'read_private_people',
				'read'              => 'read',
				'delete_posts'      => 'delete_people',
				'delete_private_posts' => 'delete_private_people',
				'delete_published_posts' => 'delete_published_people',
				'delete_others_posts' => 'delete_others_people',
				'edit_private_posts' => 'edit_private_people',
				'edit_published_posts' => 'edit_published_people',
				'create_posts'      => 'edit_people'
			),
			'hierarchical'          => false,
			'supports'              => array( 'title', 'editor', 'thumbnail' ),
			'has_archive'           => false,
			'rewrite'               => false,
			'query_var'             => true,
			'show_in_rest'          => true,
		) );

	}


}

/**
 * Returns the instance of our main Student_Affairs_People class.
 *
 * Will come in handy when we need to access the
 * class to retrieve data throughout the plugin.
 *
 * @since	1.0.0
 * @access	public
 * @return	Student_Affairs_People
 */
function student_affairs_people() {
	return Student_Affairs_People::instance();
}

// Let's get this show on the road
student_affairs_people();