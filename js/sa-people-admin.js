(function( $ ) {
    'use strict';

    // When the document is ready...
    $(document).ready(function() {

        // Set people listing department <select>
        var $department_select = $( '#sa-people-listing-department' );

        // Get departments
        $.ajax( {
            url: 'https://sa.ua.edu/wp-json/wp/v2/departments?filter[posts_per_page]=-1&filter[orderby]=title',
            success: function ( $departments ) {

                // Make sure departments are defined
                if ( $departments === undefined || $departments === null ) {
                    return false;
                }

                // Load select with departments
                $.each( $departments, function( $index, $item ) {
                    $department_select.append( '<option value="' + $item.id + '">' + $item.title.rendered + '</option>' );
                });

                // Define selected
                if ( $department_select.data( 'selected' ) !== undefined ) {

                    // Get the IDS
                    var $selected_department_ids = $department_select.data( 'selected' );

                    // Convert to array
                    var $selected_departments = [];

                    // The matching doesn't work for numbers
                    if ( $.isNumeric( $selected_department_ids ) ) {
                        $selected_departments = [$selected_department_ids];
                    } else if ( $selected_department_ids.match(/\,/gi) !== null ) {
                        $selected_departments = $selected_department_ids.split(',');
                    }

                    // Select the options
                    if ($.isArray( $selected_departments ) ) {
                        $.each($selected_departments, function ($index, $value) {
                            $department_select.find('option[value=' + $value + ']').attr('selected', true);
                        });
                    }

                }

                // Make it a select2 element
                $department_select.select2();

            },
            cache: false
        } );

    });

})( jQuery );